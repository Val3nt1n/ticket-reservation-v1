package ticketReservation;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.StringJoiner;

@Entity
@Table(schema = "tickets", name = "ticket")
public class Ticket implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "FirstName")
    private String firstName;

    @Column(name = "LastName")
    private String lastName;

    @Column(name = "phoneNumber")
    private String phoneNumber;

    @Column(name = "DateOfJourney")
    private LocalDate dateOfJourney;

    @Column(name = "TicketPrice")
    private double price;

    @Column(name = "route")
    private String route;

    @Column(name = "createdTicketTime")
    private LocalDate ticketTime;

    public Ticket() {
    }

    public Ticket(Long id, String firstName, String lastName, String phoneNumber, LocalDate dateOfJourney, LocalDate ticketTime) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.dateOfJourney = dateOfJourney;
        this.ticketTime = ticketTime;
    }


    public Ticket(Long id, String firstName, String lastName, String phoneNumber, LocalDate dateOfJourney, double price, String route, LocalDate ticketTime) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.dateOfJourney = dateOfJourney;
        this.price = price;
        this.route = route;
        this.ticketTime = ticketTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public LocalDate getDateOfJourney() {
        return dateOfJourney;
    }

    public void setDateOfJourney(LocalDate dateOfJourney) {
        this.dateOfJourney = dateOfJourney;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public LocalDate getTicketTime() {
        return ticketTime;
    }

    public void setTicketTime(LocalDate ticketTime) {
        this.ticketTime = ticketTime;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Ticket.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("phoneNumber='" + phoneNumber + "'")
                .add("dateOfJourney=" + dateOfJourney)
                .add("price=" + price)
                .add("route='" + route + "'")
                .add("ticketTime=" + ticketTime)
                .toString();
    }
}
