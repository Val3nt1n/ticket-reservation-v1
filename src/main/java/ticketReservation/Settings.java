package ticketReservation;

import javax.persistence.*;

@Entity
@Table(schema = "tickets", name = "routes")
public class Settings {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "route")
    private String route;

    @Column(name = "price")

    private double price;


    public Settings() {
    }

    public Settings(Long id, String route, double price) {
        this.id = id;
        this.route = route;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return route;
    }
}
