package ticketReservation;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

public class TicketsDTO implements Initializable {

    @FXML
    private TableView<Ticket> tableView;
    @FXML
    private TableColumn<Ticket, Integer> id;
    @FXML
    private TableColumn<Ticket, String> firstName;
    @FXML
    private TableColumn<Ticket, String> lastName;
    @FXML
    private TableColumn<Ticket, Integer> phoneNumber;
    @FXML
    private TableColumn<Ticket, LocalDate> dateOfJourney;
    @FXML
    private TableColumn<Ticket, Double> ticketPrice;
    @FXML
    private TableColumn<Ticket, LocalDate> createdTicketTime;
    @FXML
    private TableColumn<Ticket, String> route;
    @FXML
    Button backButton = new Button();


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadColumns();
        List<Ticket> ticketsFromDB = DbOperations.getTickets();
        System.out.println("~~~~~ TICKETS FROM DB ~~~~~");
        ticketsFromDB.forEach(System.out::println);
        ObservableList<Ticket> observableList = FXCollections.observableArrayList(ticketsFromDB);
        System.out.println("~~~~~ OBSERVABLE LIST OF TICKETS ~~~~~");
        observableList.forEach(System.out::println);
        tableView.setItems(observableList);
    }

    public void loadColumns() {
        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        phoneNumber.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        dateOfJourney.setCellValueFactory(new PropertyValueFactory<>("dateOfJourney"));
        ticketPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
        createdTicketTime.setCellValueFactory(new PropertyValueFactory<>("ticketTime"));
        route.setCellValueFactory(new PropertyValueFactory<>("route"));
    }


    public void backActionButton(ActionEvent actionEvent) {
        backButton.getScene().getWindow().hide();
    }

}
