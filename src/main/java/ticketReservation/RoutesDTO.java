package ticketReservation;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class RoutesDTO implements Initializable {

    @FXML
    Button backButton = new Button();

    @FXML
    TextField routeField = new TextField();

    @FXML
    TextField priceField = new TextField();

    @FXML
    Button saveButton = new Button();

    @FXML
    Label routeLabel = new Label();
    @FXML
    Label priceLabel = new Label();


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        priceField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    priceField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
    }

    public void saveRoute(ActionEvent actionEvent) {

        if (!routeField.getText().isEmpty() && !priceField.getText().isEmpty()) {
            routeLabel.setText("First Name : " + routeField.getText());
            priceLabel.setText("New Route Price Added : " + Double.parseDouble(priceField.getText()));
            DbOperations.saveNewRoute(routeField.getText(), Double.parseDouble(priceField.getText()));
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Route or Price missing");
            alert.showAndWait();
        }
    }

    public void backActionButton(ActionEvent actionEvent) {
        backButton.getScene().getWindow().hide();
    }
}
