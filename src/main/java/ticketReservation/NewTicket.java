package ticketReservation;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NewTicket implements Initializable {
    @FXML
    public Button newTicketBtn;
    @FXML
    public Button backBtn;
    @FXML
    public TextField fName;
    @FXML
    public TextField lName;
    @FXML
    public TextField phoneNumber;
    @FXML
    public DatePicker dateOfJourney;
    @FXML
    public ChoiceBox<Settings> routeChoiceBox;
    @FXML
    public Label routeLabel;
    @FXML
    public Label fNameLabel;
    @FXML
    public Label lNameLabel;
    @FXML
    public Label pNumberLabel;
    @FXML
    public Label ticketTimeLabel;
    @FXML
    public Label priceLabel;
    @FXML
    public Label dateOfJourneyLabel;
    @FXML
    public ImageView qrCode;
    @FXML
    public Button printTicketBtn;

    Document pdfTicket = new Document();

    // Print New Ticket
    public void newTicket(MouseEvent mouseEvent) {
        validateFields();
        if (!fName.getText().isEmpty() && !lName.getText().isEmpty() && !phoneNumber.getText().isEmpty() && !routeChoiceBox.getSelectionModel().isEmpty() && dateOfJourney.getValue() != null) {
            DbOperations.saveNewTicket(fName.getText(), lName.getText(), phoneNumber.getText(), dateOfJourney.getValue(), routeChoiceBox.getSelectionModel().getSelectedItem().getPrice(), routeChoiceBox.getSelectionModel().getSelectedItem().getRoute(), LocalDate.now());
            qrCode(fName.getText(), lName.getText(), phoneNumber.getText(), LocalDate.now(), dateOfJourney.getValue(), routeChoiceBox.getSelectionModel().getSelectedItem().getRoute(), routeChoiceBox.getSelectionModel().getSelectedItem().getPrice());
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Insert Problem");
            alert.showAndWait();
        }
    }


    // Print Pdf Ticket with all fields and QrCode
    public void printPdfTicket(ActionEvent actionEvent) {
        if (true) {
            try {
                PdfWriter.getInstance(pdfTicket,
                        new FileOutputStream("D:\\Tickets\\" + fName.getText() + ".pdf"));

                pdfTicket.open();
                final String string = "First Name : " + fName.getText() + "\n"
                        + "Last name : " + lName.getText() + "\n"
                        + "Passenger phonenumber : " + phoneNumber.getText() + "\n"
                        + "Created Ticket Time : " + LocalDate.now() + "\n"
                        + "Ticket Price : " + routeChoiceBox.getSelectionModel().getSelectedItem().getPrice() + " RON" + "\n"
                        + "Route : " + routeChoiceBox.getSelectionModel().getSelectedItem().getRoute() + "\n"
                        + "Date of journey : " + dateOfJourney.getValue();
                pdfTicket.add(new Paragraph(string));
                BarcodeQRCode qrcode = new BarcodeQRCode(string, 1, 1, null);
                Image qrcodeImage = qrcode.getImage();
                qrcodeImage.setAbsolutePosition(10, 500);
                qrcodeImage.scalePercent(200);
                pdfTicket.add(qrcodeImage);
                pdfTicket.close(); // no need to close PDFwriter?
                backBtn.getScene().getWindow().hide();
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "PDF ticket created in D:\\Tickets\\" + fName.getText() + ".pdf");
                alert.showAndWait();

            } catch (
                    DocumentException | FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "Failed to create PDF ticket!");
            alert.showAndWait();
        }
    }

    // Validate fields
    private void validateFields() {
        if (!routeChoiceBox.getSelectionModel().isEmpty()) {
            routeLabel.setText("Route : " + routeChoiceBox.getSelectionModel().getSelectedItem().getRoute());
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Route missing!");
            alert.showAndWait();
        }
        if (!routeChoiceBox.getSelectionModel().isEmpty()) {
            priceLabel.setText("Price : " + routeChoiceBox.getSelectionModel().getSelectedItem().getPrice() + " RON");
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Route price missing!");
            alert.showAndWait();
        }
        if (!fName.getText().isEmpty()) {
            fNameLabel.setText("First Name : " + fName.getText());
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "First Name missing!");
            alert.showAndWait();
        }
        if (!lName.getText().isEmpty()) {
            lNameLabel.setText("Last Name : " + lName.getText());
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Last Name missing!");
            alert.showAndWait();
        }
        if (!phoneNumber.getText().isEmpty()) {
            pNumberLabel.setText("Phone Number : " + phoneNumber.getText());
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Phone Number missing!");
            alert.showAndWait();
        }
        if (dateOfJourney.getValue() != null) {
            dateOfJourneyLabel.setText("Date of Journey : " + dateOfJourney.getValue());
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Date of journey missing!");
            alert.showAndWait();
        }
        ticketTimeLabel.setText("Created ticket time : " + LocalDate.now());
    }

    // generate QrCode method
    private void qrCode(String fNameString, String lNameString, String phoneNumberString, LocalDate i, LocalDate date, String routeString, double price) {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        int width = 300;
        int height = 300;
        String fileType = "png";
        BufferedImage bufferedImage = null;
        try {
            BitMatrix bitMatrix = qrCodeWriter.encode("First Name : " + fNameString + "\n"
                            + "Last Name : " + lNameString + "\n"
                            + "Phone Number : " + phoneNumberString + "\n"
                            + "Date Of Journey : " + i + "\n"
                            + "Ticket Price : " + routeChoiceBox.getSelectionModel().getSelectedItem().getPrice() + " RON" + "\n"
                            + "Route : " + routeString + "\n"
                            + "Create Ticket Time : " + date
                    , BarcodeFormat.QR_CODE, width, height);
            bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            bufferedImage.createGraphics();

            Graphics2D graphics = (Graphics2D) bufferedImage.getGraphics();
            graphics.setColor(Color.WHITE);
            graphics.fillRect(0, 0, width, height);
            graphics.setColor(Color.BLACK);

            for (int c = 0; c < height; c++) {
                for (int j = 0; j < width; j++) {
                    if (bitMatrix.get(c, j)) {
                        graphics.fillRect(c, j, 1, 1);
                    }
                }
            }

            System.out.println("Success...");

        } catch (WriterException ex) {
            Logger.getLogger(Ticket.class.getName()).log(Level.SEVERE, null, ex);
        }
        assert bufferedImage != null;
        qrCode.setImage(SwingFXUtils.toFXImage(bufferedImage, null));
    }

    // Back Button
    public void backActionButton(ActionEvent actionEvent) {
        backBtn.getScene().getWindow().hide();
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        routeChoiceBox.getItems().addAll(DbOperations.loadPriceStation());
        fName.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\D*")) {
                    fName.setText(newValue.replaceAll("[^\\D]", ""));
                }
            }
        });
        lName.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\D*")) {
                    lName.setText(newValue.replaceAll("[^\\D]", ""));
                }
            }
        });
        phoneNumber.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    phoneNumber.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        phoneNumber.setOnKeyTyped(t -> {

            int maxLength = 10;
            if (phoneNumber.getText().length() > maxLength) {
                int pos = phoneNumber.getCaretPosition();
                phoneNumber.setText(phoneNumber.getText(0, maxLength));
                phoneNumber.positionCaret(pos);
            }

        });
        BooleanBinding isTextFieldEmpty = Bindings.isEmpty(fName.textProperty())
                .or(Bindings.isEmpty(lName.textProperty()))
                .or(Bindings.isEmpty(phoneNumber.textProperty()).or(routeChoiceBox.valueProperty().isNull()).or(dateOfJourney.valueProperty().isNull()));
        // Now, bind the Button's disableProperty to that BooleanBinding
        printTicketBtn.disableProperty().bind(isTextFieldEmpty);
    }
}
