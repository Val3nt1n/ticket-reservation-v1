package ticketReservation;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReservationController implements Initializable {

    @FXML
    Button exit = new Button();

    @FXML
    Button newTicket = new Button();

    @FXML
    Button tickets = new Button();

    @FXML
    Button settings = new Button();


    public void initialize(URL url, ResourceBundle resourceBundle) {
        newTicket.setOnMouseClicked((event) -> {
            try {
                FXMLLoader loader = new FXMLLoader(new File("src/main/java/TicketReservation/NewTicket.fxml").toURI().toURL());
                Scene scene = new Scene(loader.load());
                Stage stage = new Stage();
                stage.setTitle("New Ticket Scene");
                Image image = new Image("/images/icon.png");
                stage.getIcons().add(image);
                stage.setScene(scene);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.show();
            } catch (IOException e) {
                Logger logger = Logger.getLogger(getClass().getName());
                logger.log(Level.SEVERE, "Failed to create new Window.", e);
            }
        });
        tickets.setOnMouseClicked((event) -> {
            try {
                FXMLLoader loader = new FXMLLoader(new File("src/main/java/TicketReservation/Tickets.fxml").toURI().toURL());
                Scene scene = new Scene(loader.load(), 843, 649);
                Stage stage = new Stage();
                stage.setTitle("Tickets Scene");
                Image image = new Image("/images/icon.png");
                stage.getIcons().add(image);
                stage.setScene(scene);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.show();
            } catch (IOException e) {                Logger logger = Logger.getLogger(getClass().getName());
                logger.log(Level.SEVERE, "Failed to create new Window.", e);
            }
        });
        settings.setOnMouseClicked((event) -> {
            try {
                FXMLLoader loader = new FXMLLoader(new File("src/main/java/TicketReservation/Settings.fxml").toURI().toURL());
                Scene scene = new Scene(loader.load(), 843, 649);
                Stage stage = new Stage();
                stage.setTitle("Settings Scene");
                Image image = new Image("/images/icon.png");
                stage.getIcons().add(image);
                stage.setScene(scene);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.show();
            } catch (IOException e) {
                Logger logger = Logger.getLogger(getClass().getName());
                logger.log(Level.SEVERE, "Failed to create new Window.", e);
            }
        });
    }


    public void exitActionButton(ActionEvent actionEvent) {
        System.exit(0);
    }

}


