package ticketReservation;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

public class DbOperations {
    public final static SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    DbOperations() {
    }



    public static List<Settings> loadPriceStation() {
        assert sessionFactory != null;
        try (Session session = sessionFactory.openSession()) {
            Query<Settings> query1 = session.createQuery("from Settings", Settings.class);
            //return (List<Settings>) query1.list();
            return query1.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }




    public static List<Ticket> getTickets() {
        assert sessionFactory != null;
        try (Session session = sessionFactory.openSession()) {
            Query<Ticket> result = session.createQuery("from Ticket", Ticket.class);
            return result.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }


    public static void saveNewRoute(String route, double priceField) {
        Transaction transaction = null;

        assert sessionFactory != null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(new Settings(null, route, priceField));
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) transaction.rollback();
        }
    }


    public static void saveNewTicket(String fName, String lName, String phoneNumber, LocalDate dateOfJourney, double price, String route, LocalDate timeTicket) {
        Transaction transaction = null;
        assert sessionFactory != null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(new Ticket(null, fName, lName, phoneNumber, dateOfJourney, price, route, timeTicket));
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) transaction.rollback();
        }
    }


}
